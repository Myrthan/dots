set nocompatible              " required
filetype off                  " required
" set the runtime path to include Vundle and initialize

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')
" Plugin 'VundleVim/Vundle.vim'

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
" Plugin 'scrooloose/syntastic'
Plugin 'nvie/vim-flake8'
Plugin 'jnurmine/Zenburn'
Plugin 'altercation/vim-colors-solarized'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'kien/ctrlp.vim'
Plugin 'crusoexia/vim-monokai'
Plugin 'Chiel92/vim-autoformat'
Plugin 'junegunn/vim-easy-align'
Plugin 'scrooloose/nerdcommenter'

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)
" Bundle 'Valloric/YouCompleteMe'
Bundle 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
Bundle 'Igorjan94/codeforces.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

filetype plugin on
syntax on
set number
set tabstop=4
" " when indenting with '>', use 4 spaces width
set shiftwidth=4
" " On pressing tab, insert 4 spaces
set expandtab

set encoding=utf-8

let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>
set mouse=nicr

let python_highlight_all=1
syntax on

let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree



syntax on
colorscheme monokai

set nu
set clipboard=unnamed

if has('python')
"python with virtualenv support
py << EOF
import os
import os.path, sys
import vim
if 'VIRTUAL_ENV' in os.environ:
	project_base_dir = os.environ['VIRTUAL_ENV']
	sys.path.insert(0, project_base_dir)
	activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
	execfile(activate_this, dict(__file__=activate_this))
	python_version = os.listdir(project_base_dir + '/lib')[0]
	site_packages = os.path.join(project_base_dir, 'lib', python_version, 'site-packages')
	current_directory = os.getcwd()

	sys.path.insert(1, site_packages)
	sys.path.insert(1, current_directory)
EOF
endif

set backspace=indent,eol,start
inoremap <C-s> <Esc>:w<CR>a
nnoremap <C-s> :w<CR>a

source /Users/Myrthan/Library/Python/2.7/lib/python/site-packages/powerline/bindings/vim/plugin/powerline.vim
set laststatus=2
set clipboard=unnamed

"autocmd vimenter * NERDTree
nmap <silent> <C-D> :NERDTreeToggle<CR>
:imap jj <Esc> 

let mapleader = ","


let g:CodeForcesUsername = 'myrthan'
let g:CodeForcesPassword = 'eomer44'
let g:CodeForcesTemplate = '~/TCS/codeforces/template.cpp'
let g:CodeForcesLang = 'eng'
