return {
  -- swap arguments and things
  {
    "mizlan/iswap.nvim",
    keys = {
      { "gw", ":ISwapWithRight<cr>", desc = "Swap two arguments" },
      { "<leader>is", ":ISwap<cr>", desc = "Swap many arguments" },
    },
    opts = {
      keys = "arstdhneio",
    },
  },

  { "echasnovski/mini.pairs", enabled = false },

  -- surround
  {
    "echasnovski/mini.surround",
    opts = {
      mappings = {
        add = "<space>sa", -- Add surrounding in Normal and Visual modes
        delete = "<space>sd", -- Delete surrounding
        find = "<space>sf", -- Find surrounding (to the right)
        find_left = "<space>sF", -- Find surrounding (to the left)
        highlight = "<space>sh", -- Highlight surrounding
        replace = "<space>sr", -- Replace surrounding
        update_n_lines = "<space>sn", -- Update `n_lines`
      },
    },
  },
  {
    { "echasnovski/mini.cursorword", version = false },
    opts = {},
  },
  -- syntax parser
  {
    "nvim-treesitter/nvim-treesitter",
    ---@type TSConfig
    opts = {
      highlight = {
        disable = function(_, bufnr) -- Disable in large buffers
          return vim.api.nvim_buf_line_count(bufnr) > 10000
        end,
      },
      -- stylua: ignore
      ensure_installed = {
        "scala", "haskell", "rust", "scss", "dart", "hocon", "bash", "vimdoc", "html", "javascript", "json", "lua",
        "luap",
        "markdown", "markdown_inline", "python", "query", "regex", "tsx", "typescript", "vim", "yaml", "c", "cpp"
      },
    },
  },
  {
    "nvim-treesitter/nvim-treesitter",
    opts = function(_, opts)
      if type(opts.ensure_installed) == "table" then
        vim.list_extend(opts.ensure_installed, { "c", "cpp" })
      end
    end,
  },
  {
    "madskjeldgaard/cppman.nvim",
    keys = {
      {
        "<leader>ck",
        function()
          local cppman = require("cppman")
          -- cppman.setup()

          -- Make a keymap to open the word under cursor in CPPman
          cppman.open_cppman_for(vim.fn.expand("<cword>"))
        end,
        desc = "Cppman: open the word",
      },
      {
        "<leader>cc",
        function()
          local cppman = require("cppman")
          -- Open search box
          cppman.input()
        end,
        desc = "Cppman: open the search box",
      },
    },
  },
}
