vim.o.ignorecase = true
vim.o.smartcase = true

-- bootstrap lazy.nvim, LazyVim and your plugins
require("config.lazy")
