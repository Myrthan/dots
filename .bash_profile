export PATH="$HOME/Library/Haskell/bin:$PATH"

# added by Anaconda3 4.3.0 installer
# export PATH="/Users/Myrthan/TCS/anaconda3/bin:$PATH"  # commented out by conda initialize

# added by Anaconda3 4.3.1 installer
# export PATH="/Users/Myrthan/TCS/anaconda3/bin:$PATH"  # commented out by conda initialize

# added by Anaconda2 4.3.1 installer
export PATH="/Users/Myrthan/anaconda3/bin:$PATH"

export PATH="$HOME/.cargo/bin:$PATH"

# added by Anaconda2 5.0.1 installer
export PATH="/Users/Myrthan/anaconda3/bin:$PATH"

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/Users/Myrthan/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/Users/Myrthan/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/Users/Myrthan/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/Users/Myrthan/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

